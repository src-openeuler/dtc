Name:         dtc
Version:      1.7.2
Release:      1
Summary:      Device tree compiler
License:      GPL-2.0-or-later
URL:          https://devicetree.org/
Source0:      https://www.kernel.org/pub/software/utils/%{name}/%{name}-%{version}.tar.xz
Patch6001:    backport-pylibfdt-libfdt.i-fix-backwards-compatibility-of-return-values.patch
BuildRequires: gcc make flex bison swig
BuildRequires: pkgconfig(yaml-0.1) >= 0.2.3
BuildRequires: python3-devel python3-setuptools python3-setuptools_scm python3-pip python3-wheel
Provides:      libfdt = %{version}-%{release}
Obsoletes:     libfdt < %{version}-%{release}

%description
The devicetree is a data structure for describing hardware. Rather than hard coding
every detail of a device into an operating system, many aspects of the hardware can
be described in a data structure that is passed to the operating system at boot time.
The devicetree is used by OpenFirmware, OpenPOWER Abstraction Layer (OPAL), Power
Architecture Platform Requirements (PAPR) and in the standalone Flattened Device
Tree (FDT) form.

%package      devel
Summary:      Development headers for device tree library
Requires:     libfdt = %{version}-%{release}
Provides:     libfdt-static = %{version}-%{release}
Provides:     libfdt-devel = %{version}-%{release}
Obsoletes:    libfdt-static < %{version}-%{release}
Obsoletes:    libfdt-devel < %{version}-%{release}

%description  devel
This package provides development files for dtc.

%package      -n python3-libfdt
Summary:      Python 3 bindings for device tree library
%{?python_provide:%python_provide python3-libfdt}
Requires:     %{name} = %{version}-%{release}

%description  -n python3-libfdt
This package provides python3 bindings for libfdt

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
export SETUPTOOLS_SCM_PRETEND_VERSION=%{version}

%make_build

%install
make install DESTDIR=$RPM_BUILD_ROOT PREFIX=$RPM_BUILD_ROOT/usr \
             LIBDIR=%{_libdir} BINDIR=%{_bindir} INCLUDEDIR=%{_includedir} V=1

# we don't want ftdump and it conflicts with freetype-demos, so drop it (rhbz 797805)
rm -f %{buildroot}%{_bindir}/ftdump

%check
export SETUPTOOLS_SCM_PRETEND_VERSION=%{version}
%make_build

%files
%license GPL README.license
%{_bindir}/*
%{_libdir}/libfdt.so.*

%files devel
%{_libdir}/libfdt.so
%{_includedir}/*
%{_libdir}/libfdt.a

%files -n python3-libfdt
%{python3_sitearch}/*

%files help
%doc Documentation/manual.txt

%changelog
* Thu Jan 23 2025 luckky <guodashun1@huawei.com> - 1.7.2-1
- update to 1.7.2

* Sun Sep 08 2024 Funda Wang <fundawang@yeah.net> - 1.7.1-1
- update to 1.7.1

* Thu Jul 18 2024 yingjun ni <yingjun.ni@shingroup.cn> - 1.7.0-3
- fix secure compile option error in Makefile

* Fri Jul 14 2023 dillon chen <dillon.chen@gmail.com> - 1.7.0-2
- add clang build from jammyjellyfish PR

* Fri Jul 14 2023 dillon chen <dillon.chen@gmail.com> - 1.7.0-1
- update version to 1.7.0

* Thu May 25 2023 fuanan <fuanan3@h-partners.com> - 1.6.1-3
- enable make check

* Wed Oct 26 2022 yanglongkang<yanglongkang@h-partners.com> - 1.6.1-2
- rebuild for next release

* Sat Dec 25 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> 1.6.1-1
- update version to 1.6.1

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.6.0-4
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Sat Mar 20 2021 shenyangyang<shenyangyang4@huawei.com> - 1.6.0-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Add secure compile option in Makefile

* Wed Sep 9 2020 wangchen<wangchen137@huawei.com> - 1.6.0-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Modify the URL of Source0

* Thu Apr 23 2020 chengquan3<chengquan3@huawei.com> - 1.4.7-3.h1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Update software to v1.6.0

* Tue Jan  7 2020 JeanLeo<liujianliu.liu@huawei.com> - 1.4.7-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update software package

* Thu Sep 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.4.7-2
- Package init
